import { createSlice } from "@reduxjs/toolkit";

export const cartSlice = createSlice({
    name: "cart",

    initialState: {
        products: [],
    },

    reducers: {
        addProduct: (prevState, action) => {
            const hasInCart = prevState.products.some(
                (item) => item.id === action.payload.id
            );

            if (hasInCart) return prevState;


            return {
                ...prevState,
                products: [...prevState.products, action.payload],
            }
        },
        removeProduct: (prevState, action) => {
            const product = action.payload;

            return {
                ...prevState, products: prevState.products.filter((item) => {
                    return item.id !== product.id
                })
            }
        }
    }

});

export const { addProduct, removeProduct } = cartSlice.actions;

export default cartSlice.reducer;
