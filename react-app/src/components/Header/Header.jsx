import React from "react";
import Logo from '../Logo/Logo';
import './Header.css';

import Favorite from "../Favorite/favorite";
import CartCounter from '../CartCounter/CartCounter';


const Header = () => {

    return (
        <header
            className="header"
            id="header">
            <div className="header__container container">
                <Logo />

                <div className="header__like">
                    <Favorite />
                </div>
                <CartCounter />
            </div>
        </header>
    )
}

export default Header;