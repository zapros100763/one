import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import Header from '../Header';
import Footer from '../Footer';
import './HomePage.css';

const HomePage = () => {
    return (
        <Fragment>
            <div className="page">
                <Header />            
                <div className="home-container">
                    <div className="home-text">

                        <p className="text"> Здесь должно быть содержимое главной страницы. <br /> Но в рамках курса главная страница используется лишь в демонстрационных целях</p>
                        <Link className='breadcrumbs__link' to='/product'>Перейти на страницу товара</Link>
                    </div>
                </div>                             
            </div>
            <Footer />
        </Fragment>
       
    );
}

export default HomePage;