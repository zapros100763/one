import React, { useState } from "react";

import ColorButtom from "./ColorButton.jsx";

import albumColor1 from '../../components/images/color-1.webp';
import albumColor2 from '../../components/images/color-2.webp';
import albumColor3 from '../../components/images/color-3.webp';
import albumColor4 from '../../components/images/color-4.webp';
import albumColor5 from '../../components/images/color-5.webp';
import albumColor6 from '../../components/images/color-6.webp';

import './Colors.css';

const data = [
    {
        src: albumColor1,
        alt: "красный",
        width: 31,
        height: 60,
    },
    {
        src: albumColor2,
        alt: "хаки",
        width: 45,
        height: 60,
    },
    {
        src: albumColor3,
        alt: "розовый",
        width: 31,
        height: 60,
    },
    {
        src: albumColor4,
        alt: "синий",
        width: 31,
        height: 60,
    },
    {
        src: albumColor5,
        alt: "бирюзовый",
        width: 31,
        height: 60,
    },
    {
        src: albumColor6,
        alt: "чёрный",
        width: 31,
        height: 60,
    }
];

const Colors = () => {
    const [selectedColor, setSelectedColor] = useState("бирюзовый");

    const handleColorChange = (props) => {
        const { color } = props;
        setSelectedColor(color);
    }

    return (
        <section className="colors">
            <h2 className="colors__title">Цвет товара: Синий</h2>
            <ul className="colors__list">

                {data.map((item, index) => {
                    return (
                        <ColorButtom
                            key={index}
                            src={item.src}
                            width={item.width}
                            height={item.height}
                            alt={item.alt}
                            selectedColor={selectedColor}
                            onClick={handleColorChange}
                        />
                    )
                })}
            </ul>
        </section>
    );
}

export default Colors;