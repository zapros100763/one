import React from "react";
import PropTypes from 'prop-types';
import "./ColorButton.css"

const ColorButtom = (props) => {
    const { src, width, height, alt, selectedColor, onClick } = props;
    const isActive = (selectedColor === alt);
    // let buttonClass;
    // if (isActive) {
    //     buttonClass = "colors__button colors__button_selected"
    // } else {
    //     buttonClass = "colors__button"
    // }
    // Условный (тернарный) оператор - единственный оператор в JavaScript, принимающий три операнда: условие, за которым следует знак вопроса (?), затем выражение, которое выполняется, если условие истинно, сопровождается двоеточием (:), и, наконец, выражение, которое выполняется, если условие ложно. Он часто используется в качестве укороченного варианта условного оператора if.
    const buttonClass = isActive ? "colors__button colors__button_selected" : "colors__button"

    const handleClick = () => {
        if (onClick) {
            onClick({ color: alt });
        }
    }

    return (
        <li>
            <button className={buttonClass} onClick={handleClick}>
                <img src={src} width={width} height={height} alt={alt} />
            </button>
        </li>
    );
}

export default ColorButtom;
// определяем значения наших свойств
ColorButtom.propTypes = {
    src: PropTypes.string.isRequired,
    width: PropTypes.number,
    height: PropTypes.number,
    alt: PropTypes.string,
    isActive: PropTypes.bool,
};
// определяем дефолтные св-ва
ColorButtom.defaultProps = {
    width: 31,
    height: 60
}