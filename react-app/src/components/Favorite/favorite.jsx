import React from "react";
import headerLike from './HeaderLike.svg';


const Favorite = () => {
    
return (
      <><img
        src={headerLike}
        alt="like"
        width="32px" /><span
            className="total__numberCircle likeNumber" id="likeCounter"></span></>
);
}

export default Favorite;