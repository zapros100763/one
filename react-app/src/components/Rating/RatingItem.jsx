import React from "react";

import albumStar from '../../components/images/star.png';
import albumNostar from '../../components/images/nostar.png';

const RatingItem = (props) => {
    const { isEmpty } = props;
    const imgScr = isEmpty ? albumNostar : albumStar
    return (
        <li className="review__author__raiting">
            <img className="review__author__raiting raiting" src={imgScr} />
        </li>
    );

}

export default RatingItem;