import React from "react";
import RatingItem from "./RatingItem";
import './Rating.css';


const Rating = (props) => {
    const { rating } = props;
    const emptyStarsCount = (rating < 5) ? 5 - rating : false;
    return (
        <ul className="review__author__raiting">
            {[...Array(rating)].map((item, idx) =>
                <RatingItem key={idx} isEmpty={false} />
            )}

            {emptyStarsCount &&
                [...Array(emptyStarsCount)].map((item, idx) =>
                    <RatingItem key={idx} isEmpty={true} />
                )
            }

        </ul>
    );
}
export default Rating;


