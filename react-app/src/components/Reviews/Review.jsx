import React from "react";
import Rating from "../Rating/Rating";

import './Review.css'

const Review = (props) => {
    const { autorImg,
        autorName,
        usage,
        advantages,
        disadvantages,
        rating
    } = props;
    return (
        <li className="review__author">
            <img className="review__author__avatar" src={autorImg} alt={autorName} />

            <div className="review_content">
                <h3 className="review__author__title">{autorName}</h3>
                <div className="review__author__raiting">
                    <Rating rating={rating} />
                </div>
                <div className="review__author__descriptions">
                    <div className="review__author__description">
                        <strong>Опыт использования:</strong> {usage}
                    </div>

                    <div className="review__author__description">
                        <div><strong>Достоинства:</strong></div>
                        {advantages}
                    </div>

                    <div className="review__author__description">
                        <div><strong>Недостатки:</strong></div>
                        {disadvantages}
                    </div>
                </div>
            </div>
        </li>
    );
}
export default Review;


