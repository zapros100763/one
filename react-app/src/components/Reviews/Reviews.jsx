import React from "react";
import Review from './Review';
import './Reviews.css'

import autorImg1 from '../../components/images/Mark.png';
import autorImg2 from '../../components/images/Valeri.png';


const Reviews = () => {
    const data = [
        {
            autorImg: autorImg1,
            autorName: 'Марк Г.',
            usage: 'менее месяца',
            advantages: "Это мой первый айфон после огромного количества телефонов на андроиде, всё плавно, чётко и красиво, довольно шустрое устройство, камера весьма не плохая, ширик тоже на высоте",
            disadvantages: "К самому устройству мало имеет отношение, но перенос данных с андроида-адская вещь), а если нужно  переносить фото с компа, то это только через itunes, которое урезает качество фотографий исходное",
            rating: 5
        },
        {
            autorImg: autorImg2,
            autorName: 'Валерий Коваленко',
            usage: 'менее месяца',
            advantages: "OLED Экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго",
            disadvantages: "Плохая ремонтопригодность",
            rating: 4
        }
    ];

    return (
        <section className="reviews__section reviews">
            <div className="reviews__header">
                <div className="reviews__headerWrapper">
                    <h3 className="reviews__header__reviews">Отзывы</h3>
                    <span className="reviews__header__counter"> 425</span>
                </div>
            </div>

            <ul className="review">
                {data.map((item, idx) => {
                    return (
                        <Review
                            key={idx}
                            autorImg={item.autorImg}
                            autorName={item.autorName}
                            usage={item.usage}
                            advantages={item.advantages}
                            disadvantages={item.disadvantages}
                            rating={item.rating}
                        />)
                })}
            </ul>
        </section>
    );
}
<div className="separator"></div>
export default Reviews;

