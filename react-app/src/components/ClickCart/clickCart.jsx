import React from "react";
import './clickCart.css';

const ClickCart = (props) => {

    const { isFilled, onClick } = props;
    const handleClick = () => {

        if (onClick) {
            onClick();
        }
    }

    const buttonClass = isFilled ? 'addCart__cart button button_cart button_disable' : "addCart__cart button button_cart";
    return (
        <button
            className={buttonClass}
            onClick={handleClick}
            id="buttonCart">Добавить в
            корзину</button>
    )
}

export default ClickCart;

