import React, { useState, useEffect } from "react";
import Button from "../Button/Button";
import './ReviewForm.css';

const ReviewForm = () => {
    const [errorName, setErrorName] = useState('');
    const [errorScore, setErrorScore] = useState('');

    const handleFocusName = (event) => {
        setErrorName('');
    }

    const handleFocusScore = (event) => {
        setErrorScore('');
    }

    const handleInputChange = (event) => {
        let { name, value } = event.target;
        localStorage.setItem(name, value);
    }

    const handleSubmit = (event) => {
        const nameElement = document.getElementById("reviewName");
        const scoreElement = document.getElementById("reviewScore");

        if (nameElement.value === '') {
            setErrorName('Вы забыли указать имя и фамилию');
            event.preventDefault();
            return;
        } else if (nameElement.value.length < 3) {
            setErrorName("Имя не может быть короче 2-х символов");
            event.preventDefault();
            return;
        }
        if (!(scoreElement.value > 0 && scoreElement.value <= 5)) {
            setErrorScore("Оценка должна быть от 1 до 5");
            event.preventDefault();
            return;
        }
        localStorage.setItem('name', '');
        localStorage.setItem('score', '');
        localStorage.setItem('text', '');
    }

    useEffect(() => {
        let nameElement = document.getElementById('reviewName');
        let scoreElement = document.getElementById('reviewScore');
        let textElement = document.getElementById('reviewText');

        nameElement.value = localStorage.getItem('name');
        scoreElement.value = localStorage.getItem('score');
        textElement.value = localStorage.getItem('text');
    });

    return (
        <section className="reviewFormSection">
            <h2 className="reviewFormSection__title">Добавить свой отзыв</h2>
            <form
                className="form"
                id="reviewForm"
                onSubmit={handleSubmit}>

                <div className="form__input">

                    <div className="input form__input__name">
                        <input
                            className="form__item form__itemName"
                            id="reviewName"
                            type="text"
                            name="name"
                            placeholder="Имя и фамилия"
                            onInput={handleInputChange}
                            onFocus={handleFocusName}
                        />
                        <div className="error nameError" id="errorName">{errorName}</div>
                    </div>

                    <div className="input form__input__score">
                        <input
                            className="form__item form__itemScore"
                            id="reviewScore"
                            type="number"
                            name="score"
                            placeholder="Оценка"
                            onInput={handleInputChange}
                            onFocus={handleFocusScore}
                        />
                        <div className="error scoreError" id="errorScore">{errorScore}</div>
                    </div>
                </div>

                <textarea
                    className="form__item form__itemText" id="reviewText" rows="10"
                    placeholder="Текст отзыва"
                    name='text'
                    onInput={handleInputChange}
                />
                <div>
                    <Button type="submit">Отправить отзыв</Button>
                </div>
            </form>
        </section>
    )
}

export default ReviewForm;








