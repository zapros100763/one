import React from "react";
import './Footer.css';
import Link from '../Link';
import { useCurrentDate } from "@kundinos/react-hooks";

const Footer = () => {
    const currentDate = useCurrentDate();
    const currentYear = currentDate.getFullYear();

    return (
        <footer className="footer">
            <div className="footer__copyright">
                <p className="footer__text footer__text_font_bold">&copy; &#171;<span
                    className="footer__text footer__text_colored_red">Мой</span>Маркет&#187;, 2018-{currentYear}.</p>
                <p className="footer__text">Для уточнения информации звоните по номеру
                    <Link
                        href="tel:+79000000000"
                        text=' +7 900 000 0000'
                    />
                    &#59;</p>
                <p className="footer__text">а предложения по сотрудничеству отправляйте на почту
                    <Link
                        href="mailto:partner@mymarket.com"
                        text=' partner@mymarket.com'
                    />
                </p>
            </div>

            <div className="footer__button">
                <p className="footer__text">
                    <Link
                        href="#"
                        text='Наверх'
                    />
                </p>
            </div>
        </footer>
    );
}
export default Footer;