import React, { useState } from "react";
import ConfigButton from "./ConfigButton";
import './Config.css';

const data = [
    {
        text: "128 ГБ",
    },
    {
        text: "256 ГБ",
    },
    {
        text: "512 ГБ",
    }
];

const Config = () => {
    const [selectedConfig, setselectedConfig] = useState("128 ГБ");

    const handleConfigChange = (props) => {
        const { text } = props;
        setselectedConfig(text);
    }

    return (
        <li>
            <h2 className="memory__title">Конфигурация памяти: 128 ГБ</h2>
            <ul className="memory__wrapper">
                {data.map((item, index) => {
                    return (
                        <ConfigButton
                            key={index}
                            text={item.text}
                            selectedConfig={selectedConfig}
                            onClick={handleConfigChange}
                        />
                    )
                })}
            </ul>
        </li>
    );
}


export default Config;