import React from "react";

import "./ConfigButton.css";

const ConfigButton = (props) => {
    const { text, selectedConfig, onClick } = props;
    const isActive = (selectedConfig === text);

    const buttonClass = isActive ? "memory__button memory__button_selected" : "memory__button"

    const handleClick = () => {
        if (onClick) {
            onClick({ text: text });
        }
    }

    return (
        <li>
            <button className={buttonClass} onClick={handleClick}>
                <spam>{text}</spam>
            </button>
        </li>
    );
};

export default ConfigButton;

