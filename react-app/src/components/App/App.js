import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import PageMain from '../HomePage/HomePage';
import PageProduct from '../PageProduct/PageProduct';

function App() {
  return (
    <BrowserRouter>
    <Routes>
      <Route path='/' element={<PageMain />} />
      <Route path='/product' element={<PageProduct />} />
    </Routes>
    </BrowserRouter>

  );
}
export default App;
