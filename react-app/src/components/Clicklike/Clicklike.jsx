import React from "react";
import './Clicklike.css';


const ClickLikeButton = (props) => {
    const { isFilled, onClick } = props;
    const handleClick = () => {
        if (onClick) {
            onClick();
        }
    }

    const buttonClass = isFilled ? 'like like_fullred' : 'like';

    return (
        <button
            className={buttonClass}
            id="buttonLike"
            onClick={handleClick}
        >

            <span className="visuallyHidden">Добавить в избранное</span>
            <span className="buttonLike__counter"></span>
        </button>
    )
}

export default ClickLikeButton;