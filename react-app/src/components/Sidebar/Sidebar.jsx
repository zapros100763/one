import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { addProduct } from "../../Reducers/cart-redusers";
import "./Sidebar.css";
import ClickLikeButton from '../Clicklike/Clicklike';
import ClickCartButton from "../ClickCart/clickCart";


const Sidebar = () => {

    const [clickLike, setClickLike] = useState(false);
    const [clickCart, setClickCart] = useState(false);

    const count = useSelector ((store) => store.cart.products.length); 
    const dispatch = useDispatch();   

    const handleClickLike = () => {
        setClickLike(!clickLike);

        const favoriteCounter = document.getElementById('likeCounter');
        if (clickLike) {
            favoriteCounter.innerText = '';
        } else {
            favoriteCounter.innerText = '1';
        }
    }
        
        const handleClickCart = () => {
            const action = addProduct({ id: 4884 });
            dispatch(action);

        setClickCart(!clickCart);

        const cartCounter = document.getElementById('cartCounter');
        const cartButton = document.getElementById('buttonCart')
        if (clickCart) {
            cartCounter.innerText = '';
            cartButton.innerText = 'Добавить в корзину';
        } else {
            cartCounter.innerText = '1';
            cartButton.innerText = 'Товар уже в корзине';
        }
    }

    return (
        <aside>
            <div className="conteiner__addCart addCart">
                <del className="addCart__stroke">75 990₽</del>
                <span className="addCart__discount">-8%</span>
                <div className="addCart__price">67 990₽</div>


                <p className="addCart_item">Самовывоз в четверг, 1 сентября-<strong>бесплатно</strong></p>
                <p className="addCart_item">Курьером в четверг, 1 сентября-<strong>бесплатно</strong></p>
                <ClickCartButton isFilled={clickCart} onClick={handleClickCart} />
                <ClickLikeButton isFilled={clickLike} onClick={handleClickLike} />

            </div>

            <h3 className="addCart__add">Реклама</h3>
            <div id="reclama">
                <iframe className="addCart__addIframe" src="./ads.html" frameBorder="0"></iframe>
                <iframe className="addCart__addIframe" src="./ads.html" frameBorder="0"></iframe>
            </div>
        </aside>
    );
}

export default Sidebar;