import React from "react";
import PropTypes from 'prop-types';

import './Button.css';

const Button = (props) => {
    const { type, children, onClick } = props;

    return (
        <button className="button" type={type} onClick={onClick}>{children}</button>
    )
}

export default Button;

Button.propTypes = {
    type: PropTypes.oneOf(['button', 'submit'])
};

Button.defaultProps = {
    type: 'button'
}