import React from "react";

import Header from '../Header/Header';
import Breadcrumbs from '../Breadcrumbs';
import Link from '../Link';
import Config from "../Configs/Config.jsx";
import Colors from "../Colors";
import Reviews from "../Reviews/Reviews.jsx";
import ReviewForm from "../ReviewForm/ReviewForm";
import Sidebar from "../Sidebar";
import Footer from "../Footer/Footer";

import albumImage1 from '../../components/images/image-1.webp';
import albumImage2 from '../../components/images/image-2.webp';
import albumImage3 from '../../components/images/image-3.webp';
import albumImage4 from '../../components/images/image-4.webp';
import albumImage5 from '../../components/images/image-5.webp';

import './PageProduct.css';

const PageProduct = () => {
    return (

        <div className="page">
            <Header />
            <main>
                <div className="container">
                    <div className="container__breadcrumbs">
                        <Breadcrumbs />
                    </div>

                    <h1 className="container__mainTitle mainTitle">Смартфон Apple iPhone 13, синий</h1>

                    <div className="container__gallery gallery">
                        <img className="gallery__image" src={albumImage1} alt="основной вид" />
                        <img className="gallery__image" src={albumImage2} alt="вид сзади" />
                        <img className="gallery__image" id="img3" src={albumImage3} alt="вид сбоку" />
                        <img className="gallery__image" id="img4" src={albumImage4} alt="камера" />
                        <img className="gallery__image" id="img5" src={albumImage5} alt="основной вид" />
                    </div>

                    <div className="grid">
                        <div className="grid__column grid__columnCastle">
                            <Colors />

                            <section className="memory">
                                <Config />
                            </section>

                            <section className="properties">
                                <h2 className="properties__title">Характеристики товара</h2>
                                <ul className="propertiesList">
                                    <li className="propertiesList__item">Экран: <strong>6.1</strong></li>
                                    <li className="propertiesList__item">Встроенная память: <strong>128 ГБ</strong></li>
                                    <li className="propertiesList__item">Операционная система: <strong>iOS 15</strong></li>
                                    <li className="propertiesList__item">Беспроводные интерфейсы: <strong>NFC, Bluetooth,
                                        Wi-Fi</strong></li>
                                    <li className="propertiesList__item">Процессор:
                                        <Link
                                            href="https://ru.wikipedia.org/wiki/Apple_A15"
                                            text=' Apple 15 Bionic'
                                        />
                                    </li>
                                    <li>Вес: <strong>173 г</strong></li>
                                </ul>
                            </section>

                            <section className="description">
                                <h2 className="description__title">Описание</h2>
                                <p className="description__item">Наша самая совершенная система двух камер.<br />
                                    Особый взгляд на прочность дисплея.<br />
                                    Чип, с которым всё супербыстро.<br />
                                    Аккумулятор держится заметно дольше.<br />
                                    <i>iPhone 13 - сильный мира всего.</i>
                                </p>
                                <p className="description__item">Мы разработали совершенно новую схему расположения и развернули
                                    объективы на 45 градусов.
                                    Благодаря
                                    этому
                                    внутри
                                    корпуса поместилась наша лучшая система двух камер с увеличенной матрицей широкоугольной
                                    камеры. Кроме
                                    того,
                                    мы
                                    освободили место для системы оптической стабилизации изображения сдвигом матрицы. И повысили
                                    скорость
                                    работы
                                    матрицы на сверхширокоугольной камере.</p>
                                <p className="description__item">Новая сверхширокоугольная камера видит больше деталей в тёмных
                                    областях снимков. Новая
                                    широкоугольная
                                    камера
                                    улавливает на 47% больше света для более качественных фотографий и видео. Новая оптическая
                                    стабилизация
                                    со
                                    сдвигом матрицы обеспечит чёткие кадры даже в неустойчивом положении.</p>
                                <p className="description__item">Режим «Киноэффект» автоматически добавляет великолепные эффекты
                                    перемещения фокуса и изменения
                                    резкости.
                                    Просто
                                    начните запись видео. Режим «Киноэффект» будет удерживать фокус на объекте съёмки, создавая
                                    красивый
                                    эффект
                                    размытия вокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести фокус на другого
                                    человека
                                    или
                                    объект,
                                    который появился в кадре. Теперь ваши видео будут смотреться как настоящее кино.</p>
                            </section>

                            <section className="comparison">

                                <h2 className="comparison__title">Сравнение моделей</h2>
                                <table className="comparison__table table" border="2">
                                    <thead className="table__thead">
                                        <tr className="table__thead__tr">
                                            <th className="table__th">Модель</th>
                                            <th className="table__th">Вес</th>
                                            <th className="table__th">Высота</th>
                                            <th className="table__th">Ширина</th>
                                            <th className="table__th">Толщина</th>
                                            <th className="table__th">Чип</th>
                                            <th className="table__th">Объём памяти</th>
                                            <th className="table__th">Аккумулятор</th>
                                        </tr>
                                    </thead>

                                    <tbody className="table__tbody">
                                        <tr className="table__tbody__tr">
                                            <td className="table__td">iPhone 11</td>
                                            <td className="table__td">194 грамма</td>
                                            <td className="table__td">150,9 мм</td>
                                            <td className="table__td">75,7 мм</td>
                                            <td className="table__td">8,3 мм</td>
                                            <td className="table__td">A13 Bionic chip</td>
                                            <td className="table__td">до 128 ГБ</td>
                                            <td className="table__td">До 17 часов</td>
                                        </tr>

                                        <tr className="table__tbody__tr">
                                            <td className="table__td">iPhone 12</td>
                                            <td className="table__td">164 грамма</td>
                                            <td className="table__td">146,7 мм</td>
                                            <td className="table__td">71,5 мм</td>
                                            <td className="table__td">7,4 мм</td>
                                            <td className="table__td">A14 Bionic chip</td>
                                            <td className="table__td">до 256 ГБ</td>
                                            <td className="table__td">До 19 часов</td>
                                        </tr>

                                        <tr className="table__tbody__tr">
                                            <td className="table__td">iPhone 13</td>
                                            <td className="table__td">174 грамма</td>
                                            <td className="table__td">146,7 мм</td>
                                            <td className="table__td">71,5 мм</td>
                                            <td className="table__td">7,65 мм</td>
                                            <td className="table__td">A15 Bionic chip</td>
                                            <td className="table__td">до 512 ГБ</td>
                                            <td className="table__td">До 19 часов</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </section>
                            <Reviews />
                            <ReviewForm />
                        </div>

                        <div className="grid__column grid__columnKey">
                            <Sidebar />
                        </div>
                    </div>
                </div>
            </main>
            <Footer />
        </div>
    );
}
export default PageProduct;
