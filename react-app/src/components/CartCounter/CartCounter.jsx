import React, { useState } from "react";
import { useSelector } from "react-redux";

import './CartCounter.css';
import headerBusket from './HeaderBusket.svg';

const CartCounter = () => {
    const initialProducts = [localStorage.getItem('products')];
const [initialState, setinitialState] = useState(initialProducts.length); 

const counter = useSelector ((store) => store.cart.products.length);
const counterText = ( counter === 0) ? null : counter;

    return (<div className="header__basket">
        <img
            src={headerBusket}
            alt="busket" width="32px" />
        <span
            className="total__numberCircle cartNumber" id="cartCounter">{counterText}</span>
    </div>
    )
}

export default CartCounter;