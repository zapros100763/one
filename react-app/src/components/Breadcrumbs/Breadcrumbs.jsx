import React from "react";
import './Breadcrumbs.css';
import Link from '../Link';

const Breadcrumbs = () => {
    return (
        <nav className="container__breadcrumbs">
            <Link
                href="1.html"
                text='Электроника '
            />
            <span className>&gt;</span>
            <Link
                href="2.html"
                text=' Смартфоны и гаджеты '
            />
            <span>&gt;</span>
            <Link
                href="3.html"
                text=' Мобильные телефоны '
            />
            <span>&gt;</span>
            <Link
                href="4.html"
                text=' Apple'
            />
        </nav>
    );
};

export default Breadcrumbs;