import React from "react";
import styles from './Link.module.css';


const Link = (props) => {
    const { href, text } = props;

    return (
        <a className={styles.breadcrumbs__link} href={href}>{text}</a>
    );
}

export default Link;