import React from "react";
import { Link } from "react-router-dom";
import LogoImage from './Logo.png';
import './Logo.css';

const Logo = () => {
    return (
        <div className="header__logo logo">
             <Link className="header__logo logo" to='/'>
            <img className="logo__image"
             src={LogoImage}
             alt="logo" />
            <p className="logo__caption"><span className="logo__caption_color_orange">Мой</span>Маркет</p>
            </Link>
        </div>
    );
}

export default Logo;