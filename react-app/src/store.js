import { configureStore } from "@reduxjs/toolkit";
import cartReducer from './Reducers/cart-redusers';

const logger = (store) => (next) => (action) => {
    console.log("action", action);
    let result = next(action);
    console.log("next state", store.getState());
    return result;
};

let counter = 0;
const actionCount = (store) => (next) => (action) => {
    let result = next(action);
    counter++;
    console.log("Количество обработанных действий:", counter);
    return result;
}

const storeLocalStore = (store) => (next) => (action) => {
    if (action.type === 'cart/addProduct'); {
        localStorage.setItem("products", [].push(action.payload.id));
    }

    if (action.type === 'cart/removeProduct'); {
        localStorage.setItem("products", []);
    }

    let result = next(action);
    return result;
}

export const store = configureStore({
    reducer: {
        cart: cartReducer,
    },
    middleware: [logger, actionCount, storeLocalStore],
});