// 'use strict';

// // ДЗ - 24
// let form = document.querySelector(".form");

// let formElement = document.getElementById(".review-form")

// // let inputNameConteiner = form.querySelector(".form__input__name");
// // let inputScoreConteiner = form.querySelector(".form__input__score");

// // let inputName = inputNameConteiner.querySelector('.form__item-name');
// // let errorNameElem = inputNameConteiner.querySelector(".name-error");

// // let inputScore = inputScoreConteiner.querySelector('.form__item-score');
// // let errorScoreElem = inputScoreConteiner.querySelector(".score-error");

// function handleSubmit(event) {
//     event.preventDefault();

//     // ДЗ-25.3 если пользователь отправил форму и прошёл валидацию, то считаем, что они были успешно отправлены на сервер и теперь хранить в браузере их не нужно — в таком случае после обновления страницы форма должна быть пустой
//     const clearStoreFormData = () => {
//         localStorage.removeItem('review-name');
//         localStorage.removeItem('review-score');
//         localStorage.removeItem('review-text');
//     }
//     //-------------------------------------------

//     let name = inputName.value;
//     let errorName = "";

//     if (name.length < 3) {
//         errorName = "Имя не может быть короче 2-х символов";
//     }

//     if (name.length === 0) {
//         errorName = "Вы забыли указать имя и фамилию";
//     }
//     //-----Focus Name
//     function handleRemoveError() {
//         errorNameElem.classList.remove('visible');
//     }
//     let errorNameНide = document.getElementById("review-name")
//     errorNameНide.addEventListener("focus", handleRemoveError);

//     errorNameElem.innerText = errorName;
//     errorNameElem.classList.toggle('visible', errorName);
//     if (errorName) return;
//     //------------------
//     // if (errorName) {
//     //     errorNameElem.classList.add("visible");
//     // } else {
//     //     errorNameElem.classList.remove("visible");
//     // }     
//     // вместо этого toggle!
//     //--------------

//     let score = +inputScore.value;
//     let errorScore = "";

//     if ((score < 1 || score > 5) || (score.length === 0)) {
//         errorScore = "Оценка должна быть от 1 до 5";
//     }
//     //---------------------------------------------------------------------------------------------------------------
//     // чтобы не приходилось вызывать этот обработчик вне класса, можно его по умолчанию навесить в конструкторе, рядом с определяемыми свойствами, например, так

//     // class AddReviewForm extends Form {
//     //     constructor(text, id) {
//     //         super(text);

//     //         this.form = document.querySelector(".form");
//     //         this.form.addEventListener('submit', this.handleSubmit);
//     // 	...
//     //  }
//     // Тогда, при создании экземпляра класса мы уже будем иметь форму с навешанным обработчиком отправки, что будет более удобно)
//     //---------------------------------------------------------------------------------------------------------------

//     if (name && score) clearStoreFormData();

//     //-----Focus Score
//     function handleRemoveError1() {
//         errorScoreElem.classList.remove('visible');
//     }
//     let errorScoreНide = document.getElementById("review-score")
//     errorScoreНide.addEventListener("focus", handleRemoveError1);

//     errorScoreElem.innerText = errorScore;
//     errorScoreElem.classList.toggle('visible', errorScore);
// }
// // form.addEventListener("submit", handleSubmit);

// // ДЗ-25.1

// // 1 если пользователь начал вводить какую-то информацию влюбое из полей формы, то эта информация должна быть подставлена в соответствующее поле ввода даже после перезагрузки страницы или браузера;

// // function addTooStorage(element, keyName) {
// //     element.addEventListener('keyup', () => {
// //         localStorage.setItem(keyName, element.value);
// //     });
// // }

// function storeFormData() {
//     let reviewName = document.getElementById('review-name');
//     let reviewScore = document.getElementById('review-score');
//     let reviewText = document.getElementById('review-text');

//     // addTooStorage(reviewName, 'review-name');
//     // addTooStorage(reviewScore, 'review-score');
//     // addTooStorage(reviewText, 'review-text');
// }
// // reviewName.addEventListener('keyup', () => {
// //     localStorage.setItem('review-name', reviewName.value);
// // });

// // reviewScore.addEventListener('keyup', () => {
// //     localStorage.setItem('review-score', reviewScore.value);
// // });

// // reviewText.addEventListener('keyup', () => {
// //     localStorage.setItem('review-text', reviewText.value);
// // });


// // ДЗ-25.2 если пользователь заполнил все поля формы, но не отправил её и по какой-то причине ушёл со страницы, то при следующем посещении он должен увидеть поля формы, которые уже содержат его предыдущие данные;

// function restoreFormData() {
//     let reviewNameElement = document.getElementById('review-name');
//     let reviewScoreElement = document.getElementById('review-score');
//     let reviewTextElement = document.getElementById('review-text');

//     let name = localStorage.getItem('review-name');
//     let score = localStorage.getItem('review-score');
//     let text = localStorage.getItem('review-text');

//     // reviewNameElement.value = name;
//     // reviewScoreElement.value = score;
//     // reviewTextElement.value = text;
// }
// //счётчик избранное  -------------------------------------------------------------------------
// function handleaddToLike() {
//     let likeCounter = document.getElementById("like-counter");
//     let productLike = localStorage.getItem('likeCount');

//     if (productLike === '1') {
//         localStorage.setItem('likeCount', '1');
//     }

//     likeCounter.textContent = 1;
//     localStorage.setItem('likeCount', 1);

// }

// function addToLike() {
//     // инициализация счётчика избранное
//     let productLike = localStorage.getItem('likeCount');
//     let likeCounter = document.getElementById("like-counter");
//     likeCounter.textContent = productLike;
//     // добавление в избранное
//     let addToLikeButton = document.getElementById("button-like");
//     addToLikeButton.addEventListener('click', handleaddToLike);
// }
// //счётчик корзина___________________________________________________________________

// // 3 'click'
// // находим кнопку addButton, счётчик cartCounter,
// // находим значение productCart из localStorage (ещё не записано - нет ключа и значения)
// function handleAddToCart(event) {
//     let addButton = event.target;
//     let cartCounter = document.getElementById("cart-counter");
//     let productCart = localStorage.getItem('cartCount');

//     // анализируем productCart, т.к. пока  0, пропускаем и переходим к 4
//     if (productCart === '1') {
//         // удаляем значение
//         localStorage.setItem('cartCount', '');
//         cartCounter.textContent = '';
//         addButton.textContent = 'Добавить в корзину';
//         addButton.classList.remove('button_disable');
//         // 5 выходим из функции (обработчика) -ждём следующего 'click'
//         return;
//     }
//     // 4 cartCounter записываем =1
//     cartCounter.textContent = 1;
//     // сохраняем значение в localStorage
//     localStorage.setItem('cartCount', 1);
//     addButton.textContent = 'Товар уже в корзине';
//     addButton.classList.add('button_disable');

// }
// function addToCart() {
//     // 2 инициализация счётчика
//     // получаем значение счётчика если оно есть
//     let productCart = localStorage.getItem('cartCount');
//     // находим элементы счётчика
//     let cartCounter = document.getElementById("cart-counter");
//     // присваиваем значение (в данном случае "none")
//     cartCounter.textContent = productCart;

//     // 3 добавление в корзину
//     // на кнопку добавляем обработчик 'click'
//     let addToCartButton = document.getElementById("button-cart");
//     addToCartButton.addEventListener('click', handleAddToCart);
// }
// // активная like
// function handleAddToFavorite(event) {
//     let buttonElement = event.target;
//     buttonElement.classList.toggle('like_fullred');
// }

// function addToFavorite() {
//     let favoriteButton = document.getElementById("button-like");
//     favoriteButton.addEventListener('click', handleAddToFavorite);
// }

// // 1 Обработчик загрузки окна
// // загрузка произошла  (файлы стилей, теги, изображения кроме скриптов)
// // срабатывает window.addEventListener('load')
// // срабатывает handleWindowLoad обработчик загрузки окна
// // handleWindowLoad валидирует форму, сохраняет и востанавливает значения,
// // addTo - функции, логика для like и cart
// const handleWindowLoad = () => {
//     let formElement = document.getElementById('review-form');
//     // formElement.addEventListener("submit", handleSubmit);
//     storeFormData();
//     restoreFormData();
//     addToLike();
//     addToCart();
//     addToFavorite();
// }
// window.addEventListener('load', handleWindowLoad);

