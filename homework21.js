'use strict';

// Упражнение 1
console.log("Упр №1");

/** Возвращает признак пустого объекта.
 * @param {object} obj проверяемый объект
 * @return {boolean} true, если объект пустой или false если не пустой.
 */

function isEmpty(obj) {
    for (let key in obj) {
        console.log(key);
        return false;
    }
    return true;
}
let user = {
    name: 'Ivan',
    age: 20,
}
// let empty ={}

let result = (isEmpty(user)) ? "пустой" : "не пустой";
// let result2 = (isEmpty(empty)) ? "пустой" : "не пустой";

console.log(`Объект user  ${result}`);
// console.log(`Объект empty ${result2}`);

// Упражнение 3
//Необходимо написать функциюraiseSalary(perzent),котораяпозволит произвести повышение зарплаты на определенный
//процент и будет возвращать объект с новыми зарплатами.
//Например, если мы передаем внутрь этой функции число 5, 
//тозарплата каждого сотрудника должна быть повышена на 5%(применить округление до целого числа в меньшую сторону).
//После чего необходимо вывести в консоль общий бюджетнашей команды, т.е. суммарное значение всех зарплат послеизменения.
// Добавить для этой функции описание в форматеJSDoc.
console.log("Упр №3");

// 1 вариант решения

/**
 * возвращает увеличенные з/п на заданный процент
 * @param {object} salaries - з/п всех сотрудников
 * @param {number} percent  - заданный процент увеличения з/п
 * @returns {object} - увеличенные з/п
 */
function raiseSalary(salaries, percent) {
    let salaries2 = {};

    for (let key in salaries) {
        let newSalary = Math.floor(salaries[key] + (salaries[key] * percent / 100));
        salaries2[key] = newSalary;
    }
    return salaries2;
}

let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};

let newSalaries = raiseSalary(salaries, 5);
/**
 * Суммирует з/п сотрудников
 * @param {object} salaries  - з/п всех сторуднмков
 * @returns {number}  - новый фонд з/п
 */

function getTotalSalaries(salaries) {
    let sum = 0;
    for (let key in salaries) {
        let salary = salaries[key];
        if (!isNaN(Number(salary))) {
            sum = sum + salaries[key];
        }
    }
    return sum;
}

console.log(newSalaries);
console.log(getTotalSalaries(newSalaries));


// 2 вариант решения (лектор)
// https://codesandbox.io/s/flamboyant-sara-3006mu?file=/src/index.js
// let salaries = {
//     John: 100000,
//     Ann: 160000,
//     Pete: 130000,
// };

// function raiseSalary(percent) {
// let newSalaries = {}

//     for (let key in salaries) {
//         let raise = salaries[key] * percent / 100;
//         newSalaries[key] = salaries[key] + raise;
//     }
//     return newSalaries;
// }

// let result = raiseSalary(5);

// console.log(salaries, result);