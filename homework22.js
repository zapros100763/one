'use strict';

// Упражнение 1
console.log("Упр №1");

let arr = ['a', 1, 2, { name: 'Igor', age: 1, }, 3, 4, 'привет', -5, 6, '7'];
let res = 0;

function getSumm(arr) {

    for (let i = 0; i <= arr.length; i++) {

        if (typeof arr[i] !== 'number') continue;
        res += arr[i];
    }
    return res;
}
console.log(getSumm(arr));

// Внимание! При добавлении в массив 'Nan' а в if '&& isNan(arr[i])' код не работает. Не смог разбраться почему.

// Упражнение 3
console.log("Упр №3");

let cart = [2222, 3333];
/**
 * Добавляет элементы в корзину по идентификатору
 * @param {numder} id - идентификатор товара
 * @return {Array} массив элементов корзины
 */
function addToCart(id) {
    if (!cart.includes(id)) {
        cart.push(id);
    }
    return cart;
}
/**
 * Удаляет элементы из корзины по идентификатору
 * @param {number} id - идентификатор товара
 * @return {Array} массив элементов корзины
 */

function removeFromCart(id) {
    let index = cart.indexOf(id);
    if (index >= 0) {
        cart.splice(index, 1);
    }
    return cart;
}

addToCart(5447);
removeFromCart(2222);
console.log(cart);

