'use strict';


// Упражнение 1.1

// /**
//  * Показывает счётчик
//  * @param {number} counter - время отсчёта (сек.)
//  */  

let counter = +prompt('Введите число');

if (isNaN(counter)) {
  console.log('Ошибка! Введите число!');
  }
    else {
    timer(counter);
  }

function timer(counter) {  

let interval = setInterval(() => {
  console.log(`Осталось ${counter}`);
  counter -= 1;

if (counter === 0 ) {    
    clearInterval(interval);
    console.log('Время вышло');
  }
}, 1000);
}

// Упражнение 1.2 (промис)


// let promice = new Promise((resolve, reject) => {
//   let counter = +prompt('Введите число');

//   if (isNaN(counter)) {
//     reject();
//   }

//   resolve(counter);
// });

// promice
//   .then((counter) => {
//     let interval = setInterval(() => {
//       console.log(`Осталось ${counter}`);
//       counter -= 1;

//       if (counter === 0) {
//         clearInterval(interval);
//         console.log('Время вышло');
//       }
//     }, 1000);
//   })

//   .catch(() => {
//     console.log('Ошибка! Введите число!');
//   });


console.log("Упр №2");
// Упражнение 2 (запрос на бэкенд по адресу)

let startData = new Date().getTime();
console.log(`start запроса`, startData, `ms`);

let promise = fetch("https://reqres.in/api/users");

promise
  .then((responce) => {
    return responce.json();
  })

  .then((responce) => {
    let data = responce.data;
    console.log(`получили пользователей ${data.length}`);

    for (let i = 0; i < data.length; i++) {
      let firstName = data[i].first_name;
      let lastName = data[i].last_name;
      let email = data[i].email;

      console.log(`- ${firstName} ${lastName} (${email})`);
    }

    let endData = new Date().getTime();
    console.log(`end запроса`, endData, `ms`);
    console.log(`время запроса`, endData - startData,`ms`);
  })
  .catch(() => {
    console.log(`Ошибка! Сервер недоступен!`)
  });


