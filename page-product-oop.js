'use strict';

class Form {
constructor(text) {
    this.text = text;
}
showAlert() {
    alert(this.text); 
}
}

class AddReviewForm extends Form {
    constructor(text, id) {
        super(text);

        this.form = document.querySelector(".form");

        this.inputNameConteiner = this.form.querySelector(".form__input__name");
        this.inputScoreConteiner = this.form.querySelector(".form__input__score");

        this.inputName = this.inputNameConteiner.querySelector('.form__item-name');
        this.errorNameElem = this.inputNameConteiner.querySelector(".name-error");

        this.inputScore = this.inputScoreConteiner.querySelector('.form__item-score');
        this.errorScoreElem = this.inputScoreConteiner.querySelector(".score-error");
    }

    addListener() {
        this.form.addEventListener("submit", this.handleSubmit);
    }

    handleSubmit = (event) => {
    event.preventDefault();

    const clearStoreFormData = () => {
        localStorage.removeItem('review-name');
        localStorage.removeItem('review-score');
        localStorage.removeItem('review-text');
    }

    let name = this.inputName.value;
    let errorName = "";

    if (name.length < 3) {
        errorName = "Имя не может быть короче 2-х символов";
    }

    if (name.length === 0) {
        errorName = "Вы забыли указать имя и фамилию";
    }

    this.errorNameElem.innerText = errorName;
    this.errorNameElem.classList.toggle('visible', errorName);
    if (errorName) return;

    let score = this.inputScore.value;
    let errorScore = "";

    if ((score < 1 || score > 5) || (score.length === 0)) {
        errorScore = "Оценка должна быть от 1 до 5";
    }

    this.errorScoreElem.innerText = errorScore;
    this.errorScoreElem.classList.toggle('visible', errorScore);
}
}

let reviewForm = new AddReviewForm("review-form");
reviewForm.addListener();

// let ClassForm = new Form('текст');
// ClassForm.showAlert();

// let ReviewForm = new AddReviewForm("Форма успешно отправлена!", "review-form");
// ReviewForm.showAlert();




